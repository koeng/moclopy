from setuptools import setup

setup(name='moclopy',
      version='0.1',
      description='Tools for synthetic MoClo compatible sequences',
      author='Keoni Gandall',
      author_email='koeng101@gmail.com',
      license='MIT',
      packages=['moclopy'],
      zip_safe=False)

