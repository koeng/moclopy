# MoClopy 

A suite of tools for manipulating synthetic DNA to be synthesizable and MoClo compatible.

# Installation
```
git clone https://gitlab.com/koeng/moclopy.git
cd synbiolib
pip install -e .
```

# Dependencies
- synbiolib
- biopython
